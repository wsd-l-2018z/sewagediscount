# Demo scenario

1. Start main container

```
mvn -P main exec:java
```

2. Start agent containers

```
mvn -P container exec:java -D "agent=centrum-neighborhood-agent:wsd.groupl.neighborhood.NeighborhoodAgent()"

mvn -P container exec:java -D "agent=centrum-probe-station-agent-2:wsd.groupl.probestation.ProbeStationAgent()"
mvn -P container exec:java -D "agent=centrum-probe-station-agent:wsd.groupl.probestation.ProbeStationAgent()"


mvn -P container exec:java -D "agent=centrum-shop-agent-3:wsd.groupl.shop.ShopAgent()"
mvn -P container exec:java -D "agent=centrum-shop-agent-2:wsd.groupl.shop.ShopAgent()"
mvn -P container exec:java -D "agent=centrum-shop-agent-1:wsd.groupl.shop.ShopAgent()"

mvn -P container exec:java -D "agent=first-ad-agent:wsd.groupl.advertisingSpace.AdvertisingSpaceAgent()"
mvn -P container exec:java -D "agent=first-ad-agent-2:wsd.groupl.advertisingSpace.AdvertisingSpaceAgent()"
```

Alternative 2.
```
mvn -P container exec:java -D "agent=centrum-neighborhood-agent:wsd.groupl.neighborhood.NeighborhoodAgent()"
mvn -P container exec:java -D "agent=mokotow-neighborhood-agent:wsd.groupl.neighborhood.NeighborhoodAgent()"

mvn -P container exec:java -D "agent=centrum-probe-station-agent-2:wsd.groupl.probestation.ProbeStationAgent()"
mvn -P container exec:java -D "agent=mokotow-probe-station-agent-2:wsd.groupl.probestation.ProbeStationAgent()"

mvn -P container exec:java -D "agent=centrum-shop-agent-3:wsd.groupl.shop.ShopAgent()"
mvn -P container exec:java -D "agent=centrum-shop-agent-2:wsd.groupl.shop.ShopAgent()"
mvn -P container exec:java -D "agent=mokotow-shop-agent-3:wsd.groupl.shop.ShopAgent()"
mvn -P container exec:java -D "agent=mokotow-shop-agent-2:wsd.groupl.shop.ShopAgent()"

mvn -P container exec:java -D "agent=first-ad-agent:wsd.groupl.advertisingSpace.AdvertisingSpaceAgent()"


```
