package wsd.groupl.neighborhood.store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
//we store all old data so we can later extend logic
public class DataAggregator {
	private int iteration = 1;
	private Map<String, ArrayList<Double>> data = new HashMap<String, ArrayList<Double>>();
	private String product;
	private String winingShop;
	private String winingProduct;
	private Double bestPrice; 
	
	private static DataAggregator instance = null;
	public static DataAggregator getInstance() {
		if(instance == null) {
				instance = new DataAggregator();
		}
		return instance;
	}
	protected DataAggregator() {}
	
	public void addData(Map<String, Object> map) {
		map.forEach((k,v) -> {
			if(data.containsKey(k)) {
				data.get(k).add((Double)v);
			} else {
				ArrayList<Double> newArray = new ArrayList<Double>();
				newArray.add((Double)v);
				data.put(k, newArray);
			}
		});
		iteration++;
	}
	
	public Double getData(String biologicalIndicator) {
		return data.get(biologicalIndicator).stream().mapToDouble(a -> a).sum() / iteration;
	}
	
	public Map<String, Double> getAll(){
		Map<String, Double> result = new HashMap<String, Double>();
		data.forEach((k,v) -> {
			result.put(k, v.stream().mapToDouble(a -> a).sum() / iteration);
		});
		return result;
	}
	
	public Map<String, Double> getLastAll(int probes) {
		Map<String, Double> result = new HashMap<String, Double>();
		data.forEach((k,v) -> {
			result.put(k, v.stream().filter(o -> v.indexOf(o) > (v.size() - probes)).mapToDouble(a -> a).sum() / iteration);
		});
		return result;
	}
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public void deleteMissingProduct() {
		this.product = null;
	}
	
	public String getWiningShop() {
		return winingShop;
	}
	public void setWiningShop(String winingShop, String winingProduct, Double bestPrice) {
		this.winingShop = winingShop;
		this.setWiningProduct(winingProduct);
		this.setBestPrice(bestPrice);
	}
	
	public void reset() {
		iteration = 1;
		data = new HashMap<String, ArrayList<Double>>();
	}
	
	public String getWiningProduct() {
		return winingProduct;
	}
	
	public void setWiningProduct(String winingProduct) {
		this.winingProduct = winingProduct;
	}
	
	public Double getBestPrice() {
		return bestPrice;
	}
	
	public void setBestPrice(Double bestPrice) {
		this.bestPrice = bestPrice;
	}
	
}
