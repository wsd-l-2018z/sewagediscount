package wsd.groupl.neighborhood.store;

import java.util.HashMap;
import java.util.Map;

public class DataAnalyzer {
	private DataAggregator dataAggregator;
	private Map<String, Double> biologicalIndicators;
	private ProductMappings productMappings;
	private Double min = 100d;
	public DataAnalyzer() {
		productMappings = new ProductMappings();
		dataAggregator = DataAggregator.getInstance();
		biologicalIndicators = new HashMap<String, Double>();
		biologicalIndicators.put("pH", 50d);
	    biologicalIndicators.put("magnesium", 50d);
	    biologicalIndicators.put("potassium", 50d);
	    biologicalIndicators.put("iron", 50d);
	    biologicalIndicators.put("calcium", 50d);
	}
	
	public String getMissingProduct() {
		//getting last n elements from data Aggregator
		min = 100d;
		Map<String, Double> aggregatedData = dataAggregator.getLastAll(2);
		String result = null;
		for (Map.Entry<String, Double> entry : aggregatedData.entrySet())
		{
		    if(entry.getValue() < biologicalIndicators.get(entry.getKey()).doubleValue()) {
		    	if(min > entry.getValue()) {
			    	result = entry.getKey();
			    	min = entry.getValue();
		    	}
		    }
		}
		if(result != null) {
			return productMappings.getProduct(result);
		}
		return null;
	}
}
