package wsd.groupl.neighborhood.store;

import java.util.HashMap;
import java.util.Map;

public class ProductMappings {

    private Map<String, String> productsMapping;

    public ProductMappings() {
        initProducts();
    }

    private void initProducts() {
    	productsMapping = new HashMap<String, String>();
    	productsMapping.put("magnesium", "TOMATO");
    	productsMapping.put("potasium", "BANANA");
    	productsMapping.put("pH", "BROCCOLI");
    	productsMapping.put("calcium", "APPLE");
    	productsMapping.put("iron", "HAM");
    }
    
    public String getProduct(String biologicalIndicator) {
    	return productsMapping.get(biologicalIndicator);
    }
}