package wsd.groupl.neighborhood;

import wsd.groupl.neighborhood.behaviour.AdProviderBehaviour;
import wsd.groupl.neighborhood.behaviour.AuctionerBehaviour;
import wsd.groupl.neighborhood.behaviour.DataAggregatorBehaviour;
import wsd.groupl.neighborhood.behaviour.DataAnalyzerBehaviour;
import wsd.groupl.probestation.behaviours.CollectDataBehaviour;
import wsd.groupl.shared.Logger;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class NeighborhoodAgent extends Agent {

    @Override
    protected void setup() {
        System.out.println("Agent " + getAID().getName() + " is ready.");
        addBehaviour(new DataAggregatorBehaviour());
        Integer time = Integer.parseInt(getProperty("analyzer-waiting-time", "20000"));
        addBehaviour(new DataAnalyzerBehaviour(this, time) {
            @Override
            protected void onTick() {
            	String product = analyze();
            	if(product != null) {
                    addBehaviour(new AuctionerBehaviour(discoverNeighbourhoodAgents()));
            	}

            }
        });
        addBehaviour(new AdProviderBehaviour());
    }

    @Override
    protected void takeDown() {
        System.out.println("Agent " +getAID().getName() + " is dead");
    }
    
    private List<AID> discoverNeighbourhoodAgents() {
    	ArrayList<AID> agentsId = new ArrayList<AID>();
    	AMSAgentDescription [] agents = null;

        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this, new AMSAgentDescription (), c );
        }
        catch (Exception e) {
        	
        }
        String shopList = "Available shops:  ";
        for (int i=0; i<agents.length;i++){
            AID agentID = agents[i].getName();
            String localName = agentID.getLocalName();
        	String myName = this.getLocalName().split("-")[0];
            String agentName = myName + "-shop-agent";
            if(localName.startsWith(agentName)) {
                agentsId.add(agentID);
                shopList += " " + agentID.getLocalName();
            }
       }
        Logger.log(shopList);
        
    	return agentsId;
    }

}
