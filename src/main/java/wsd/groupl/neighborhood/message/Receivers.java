package wsd.groupl.neighborhood.message;

import jade.core.AID;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Receivers {
	public Receivers() {}
	public MessageTemplate gatheredDataTemplete() {
		return MessageTemplate.MatchPerformative( ACLMessage.INFORM );
	}
	public MessageTemplate offerRecieverTemplete(String agentName) {
		return MessageTemplate.and(MessageTemplate.MatchPerformative( ACLMessage.PROPOSE ),MessageTemplate.MatchSender( new AID(agentName, AID.ISLOCALNAME))) ;
	}
	public MessageTemplate adRecieverTemplete() {
		return MessageTemplate.MatchPerformative( ACLMessage.REQUEST ) ;
	}
}
