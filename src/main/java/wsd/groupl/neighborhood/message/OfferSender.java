package wsd.groupl.neighborhood.message;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class OfferSender {
    private Agent agent;

    public OfferSender(Agent agent) {
        this.agent = agent;
    }

	public void send(ACLMessage msg) {
		agent.send(msg);
	}

	public ACLMessage getNewMessage(String product) {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
    	msg.setContent(product);
		return msg;
	}

}