package wsd.groupl.neighborhood.message;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

public class AdSender {

    public AdSender() {}

    public ACLMessage getMessage(String adContent, String receiver) {
        ACLMessage propose = new ACLMessage(ACLMessage.INFORM);
        AID receiverId = new AID(receiver, true);
        propose.addReceiver(receiverId);
        propose.setContent(adContent);
        return propose;
    }

}
