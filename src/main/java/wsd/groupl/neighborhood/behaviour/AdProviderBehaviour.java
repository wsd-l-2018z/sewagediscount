package wsd.groupl.neighborhood.behaviour;


import org.json.JSONObject;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.neighborhood.message.AdSender;
import wsd.groupl.neighborhood.message.OfferSender;
import wsd.groupl.neighborhood.message.Receivers;
import wsd.groupl.neighborhood.store.DataAggregator;
import wsd.groupl.shared.Logger;

public class AdProviderBehaviour  extends CyclicBehaviour {
    private AdSender adSender;
    private DataAggregator dataAggregator;
    private Receivers receivers;

    public AdProviderBehaviour() {
    	dataAggregator = DataAggregator.getInstance();
    	receivers = new Receivers();
    }
    
    public void onStart() {
    	adSender = new AdSender();
    }

    public void action() {
        ACLMessage msg = myAgent.receive(receivers.adRecieverTemplete());
        if (msg != null) {
        	JSONObject data = new JSONObject();
        	data.put("winingShop", dataAggregator.getWiningShop());
        	data.put("bestPrice", dataAggregator.getBestPrice());
        	data.put("winingProduct", dataAggregator.getWiningProduct());
        	Logger.log("Advertisment data: ( Missing Product: " + dataAggregator.getWiningProduct() + " Best Price $" + dataAggregator.getBestPrice() + ". Wining shop -> " + dataAggregator.getWiningShop() +" ) to: " + myAgent.getLocalName());
        	ACLMessage message = adSender.getMessage(data.toString(), msg.getSender().getName());
            myAgent.send(message);
        } else {
            block();
        }
    }
}
