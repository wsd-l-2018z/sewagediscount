package wsd.groupl.neighborhood.behaviour;


import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.neighborhood.message.OfferSender;
import wsd.groupl.neighborhood.message.Receivers;
import wsd.groupl.neighborhood.store.DataAggregator;
import wsd.groupl.shared.Logger;

public class DataAggregatorBehaviour extends CyclicBehaviour {
	private DataAggregator dataAggregator;
    private OfferSender offerSender;
    private Receivers receivers;

    public DataAggregatorBehaviour() {
        offerSender = new OfferSender(myAgent);
        dataAggregator = DataAggregator.getInstance();
        receivers = new Receivers();
    }

    public void action() {
        ACLMessage msg = myAgent.receive(receivers.gatheredDataTemplete());
        if (msg != null) {
           String data = msg.getContent();
           Logger.log("Recieved data from: " + msg.getSender().getLocalName() + " data: " + data);
           //parsing string to hash map
           JSONObject json = new JSONObject(data);
           dataAggregator.addData(json.toMap());
        } else {
            block();
        }
    }
}