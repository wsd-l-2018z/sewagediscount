package wsd.groupl.neighborhood.behaviour;

import java.util.HashMap;
import java.util.Map;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.neighborhood.message.OfferSender;
import wsd.groupl.neighborhood.store.DataAggregator;
import wsd.groupl.neighborhood.store.DataAnalyzer;

public abstract class DataAnalyzerBehaviour extends TickerBehaviour{
	private DataAnalyzer dataAnalyzer;
	private DataAggregator dataAggregator;
	
	protected DataAnalyzerBehaviour(Agent a, long period) {
    	super(a, period);
    	dataAnalyzer = new DataAnalyzer();
    	dataAggregator = DataAggregator.getInstance();
    }
	protected String analyze() {
    	String product = dataAnalyzer.getMissingProduct();
    	if(product != null) {
        	dataAggregator.setProduct(product);
    	}
    	return product;
    }

}
