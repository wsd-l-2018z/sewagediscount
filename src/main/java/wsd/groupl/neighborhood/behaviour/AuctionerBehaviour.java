package wsd.groupl.neighborhood.behaviour;


import java.util.List;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.neighborhood.message.AdSender;
import wsd.groupl.neighborhood.message.OfferSender;
import wsd.groupl.neighborhood.message.Receivers;
import wsd.groupl.neighborhood.store.DataAggregator;
import wsd.groupl.shared.Logger;

public class AuctionerBehaviour extends SequentialBehaviour {
	Double bestPrice = 9999d;
	ACLMessage bestOffer = null;
	DataAggregator dataAggregator;
    private OfferSender offerSender;
    private List<AID> agentsList;
    private Receivers receivers;

    
    public AuctionerBehaviour(List<AID> list) {
    	agentsList = list;
    	dataAggregator = DataAggregator.getInstance();
    	receivers = new Receivers();
    }
    
    public void onStart() {
    	bestPrice = 9999d;
    	bestOffer = null;
    	offerSender = new OfferSender(myAgent);
    	String product = dataAggregator.getProduct();
    	if(product != null) {
        	ACLMessage msg = offerSender.getNewMessage(product);
        	
        	ParallelBehaviour par = new ParallelBehaviour( ParallelBehaviour.WHEN_ALL );
            this.addSubBehaviour( par );
            
            for (AID agent: agentsList) 
            {
               msg.addReceiver(agent);
               par.addSubBehaviour( new MyReceiver(myAgent, 10000, receivers.offerRecieverTemplete(agent.getLocalName())) {
                     public void handle( ACLMessage msg){  
                        if (msg != null) {
                        	Double offer = Double.parseDouble( msg.getContent());
                           if (offer < bestPrice) {
                              bestPrice = offer;
                              bestOffer = msg;
                           }
                           
                        }  
                     }
                  });
            }
            this.addSubBehaviour(new OneShotBehaviour() {
    	       public void action() 
    	       {  
    	          if (bestOffer != null) {
    	        	  dataAggregator.setWiningShop(bestOffer.getSender().getLocalName(), dataAggregator.getProduct(), bestPrice);
    	              Logger.log("Missing Product: " + dataAggregator.getProduct() + " Best Price $" + bestPrice + ". Wining shop -> " + bestOffer.getSender().getLocalName() );
    	        	  dataAggregator.deleteMissingProduct();
    	          }
    	       }
            });
            
            offerSender.send(msg);
    	}
    }
}