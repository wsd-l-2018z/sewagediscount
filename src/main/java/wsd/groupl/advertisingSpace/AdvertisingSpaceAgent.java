package wsd.groupl.advertisingSpace;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import wsd.groupl.advertisingSpace.behaviours.AdvertismentReceiver;
import wsd.groupl.advertisingSpace.behaviours.AdvertismentSender;
import wsd.groupl.advertisingSpace.store.LocationProvider;
import wsd.groupl.shared.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AdvertisingSpaceAgent extends Agent {

    private LocationProvider locationProvider;

    @Override
    protected void setup(){
        System.out.println("Agent " + getAID().getName() + " is ready.");
        Integer time = Integer.parseInt(getProperty("request-waiting-time", "20000"));
        addBehaviour(new AdvertismentSender(this, time) {
            @Override
            protected void onTick() {
                locationProvider = new LocationProvider();
                AID neighboor = locationProvider.randomNeighboor(findNeighborhoodAgents());
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.addReceiver(neighboor);
                msg.setContent("Send Advertisment");
                myAgent.send(msg);
            }
        });
        addBehaviour(new AdvertismentReceiver());
    }

    @Override
    protected void takeDown() {

        System.out.println("Agent " +getAID().getName() + " is dead");
    }

    private List<AID> findNeighborhoodAgents(){

        ArrayList<AID> agentsId = new ArrayList<AID>();
        AMSAgentDescription[] agents = null;

        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( new Long(-1) );
            agents = AMSService.search( this, new AMSAgentDescription (), c );
        }
        catch (Exception e) {

        }

        String neighborhoodList = "Available neigborhood Agents:  ";
        for (int i=0; i<agents.length;i++) {
            AID agentID = agents[i].getName();
            String localName = agentID.getLocalName();

            if (localName.contains("neighborhood-agent")) {
                agentsId.add(agentID);
                neighborhoodList += " " + localName;
            }
        }
        Logger.log(neighborhoodList);

        return agentsId;
    }
}
