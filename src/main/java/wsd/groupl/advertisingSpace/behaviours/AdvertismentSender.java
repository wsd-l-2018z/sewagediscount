package wsd.groupl.advertisingSpace.behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.advertisingSpace.store.LocationProvider;

import java.util.List;
import java.util.UUID;

public abstract class AdvertismentSender extends TickerBehaviour {

    public AdvertismentSender(Agent a, long period) {
        super(a, period);
    }
}
