package wsd.groupl.advertisingSpace.behaviours;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;
import wsd.groupl.shared.Logger;

import static wsd.groupl.advertisingSpace.message.Receivers.adRecieverTemplete;

public class AdvertismentReceiver extends CyclicBehaviour  {

    public void action() {

            ACLMessage msg = myAgent.receive(adRecieverTemplete());
            if (msg!= null){
            	JSONObject json = new JSONObject(msg.getContent());
            	json.get("winingProduct");
            	json.get("bestPrice");
            	json.get("winingShop");
                Logger.log("Recived ad details. Shop: " + json.get("winingShop") + " Product: " + json.get("winingProduct") + " Price:  " + json.get("bestPrice") + " from: " + msg.getSender().getLocalName() );

        }
    }
}
