package wsd.groupl.advertisingSpace.store;

import jade.core.AID;
import wsd.groupl.shared.Logger;

import java.util.List;
import java.util.Random;

public class LocationProvider {

    public AID randomNeighboor(List<AID> neighborhoodList) {
        int pick = new Random().nextInt(neighborhoodList.size());
        AID neighboorAgent = neighborhoodList.get(pick);
        Logger.log("My neighboor: " + neighboorAgent.getLocalName());
        return neighboorAgent;
    }
}
