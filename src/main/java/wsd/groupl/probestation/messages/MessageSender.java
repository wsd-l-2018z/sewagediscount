package wsd.groupl.probestation.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class MessageSender {
    private final ACLMessage message;

    private MessageSender(int performative) {
        this.message = new ACLMessage(performative);
    }

    public static MessageSender inform() {
        return new MessageSender(ACLMessage.INFORM);
    }

    public MessageSender toLocal(String receiver) {
        AID receiverId = new AID(receiver, AID.ISLOCALNAME);
        message.addReceiver(receiverId);
        return this;
    }

    public MessageSender withContent(Map<String, Double> map) {
        JSONObject json = new JSONObject(map);
        message.setContent(json.toString());
        return this;
    }

    public ACLMessage build() {
        return message;
    }
}
