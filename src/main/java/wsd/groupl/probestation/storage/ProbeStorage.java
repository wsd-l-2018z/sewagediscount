package wsd.groupl.probestation.storage;

import java.util.HashMap;
import java.util.Map;

public class ProbeStorage {
	private Map<String, Double> data;
	private int iteration = 1;
	
	private static ProbeStorage instance = null;
	public static ProbeStorage getInstance() {
		if(instance == null) {
				instance = new ProbeStorage();
		}
		return instance;
	}
	protected ProbeStorage() {}
	
	public void addData(Map <String, Double> newData) {
		if(iteration == 1) {
			data = new HashMap<String, Double>();
		}
		newData.forEach((k,v) -> {
			if(data.containsKey(k)) {
				data.replace(k, (data.get(k) * iteration + v) / iteration + 1);
			}
			else {
				data.put(k, v);
			}
		});
		iteration++;
	}
	
	public Map<String, Double> getData(){
		iteration = 1;
		return data;
	}
	
	public Boolean anyData() {
		return iteration > 1;
	}
}