package wsd.groupl.probestation;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import wsd.groupl.probestation.behaviours.CollectDataBehaviour;
import wsd.groupl.probestation.behaviours.SendMessageBehaviour;

import java.util.HashMap;

public class ProbeStationAgent extends Agent {
    private HashMap<String, Double> indicators;

    @Override
    protected void setup() {
        System.out.println("Hello! Agent " + getAID().getName() + " is ready.");
        //registerService();
        Integer time = Integer.parseInt(getProperty("data-collector-waiting-time", "10000"));
        addBehaviour(new CollectDataBehaviour(this, time) {
            @Override
            protected void onTick() {
                probeSewage();
                saveBiologicalIndicators();

            }
        });
    	String myName = this.getLocalName().split("-")[0];
        String neighbourhoodAgent = getProperty("neighbourhood", myName + "-neighborhood-agent");
        addBehaviour(new SendMessageBehaviour(neighbourhoodAgent) {
            @Override
            public boolean done() {
                return false;
            }
        });
    }

    private void registerService() {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("sewage-prober");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        System.out.println("Agent " + getAID().getName() + " terminating.");
    }
}
