package wsd.groupl.probestation.behaviours;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import wsd.groupl.probestation.storage.ProbeStorage;
import wsd.groupl.shared.Logger;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.util.HashMap;

public abstract class CollectDataBehaviour extends TickerBehaviour {
    private HashMap<String, Double> biologicalIndicators = new HashMap<String, Double>();
    private String agent;
    private ProbeStorage probeStorage;

    protected void saveBiologicalIndicators() {
        biologicalIndicators.put("pH", this.pH);
        biologicalIndicators.put("magnesium", this.magnesium);
        biologicalIndicators.put("potassium", this.potassium);
        biologicalIndicators.put("iron", this.iron);
        biologicalIndicators.put("calcium", this.calcium);
        this.probeStorage.addData(biologicalIndicators);
        Logger.log("Collecting data: " + biologicalIndicators.toString());
    }

    private double pH;
    private double magnesium;
    private double potassium;
    private double iron;
    private double calcium;

    protected CollectDataBehaviour(Agent a, long period) {
        super(a, period);
        this.probeStorage = ProbeStorage.getInstance();
    }
    
    protected void probeSewage() {
        this.pH = generateData();
        this.magnesium = generateData();
        this.potassium = generateData();
        this.iron = generateData();
        this.calcium = generateData();
    }

    private double generateData() {
        return Math.random() * 100;
    }
}
