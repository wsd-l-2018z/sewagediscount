package wsd.groupl.probestation.behaviours;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.neighborhood.store.DataAggregator;
import wsd.groupl.neighborhood.store.DataAnalyzer;
import wsd.groupl.probestation.storage.ProbeStorage;
import wsd.groupl.shared.Logger;

import java.util.HashMap;

import static wsd.groupl.probestation.messages.MessageSender.inform;

public abstract class SendMessageBehaviour extends Behaviour {
    private String agent;
    private ProbeStorage probeStorage;

    protected SendMessageBehaviour(String agent) {
        this.agent = agent;
        this.probeStorage = ProbeStorage.getInstance();
    }
    public void action() {
    	if(this.probeStorage.anyData()) {
    		ACLMessage message = inform().toLocal(agent).withContent(this.probeStorage.getData()).build();
            Logger.log("Sending data: " + this.probeStorage.getData().toString());
            myAgent.send(message);
    	}
    }
}
