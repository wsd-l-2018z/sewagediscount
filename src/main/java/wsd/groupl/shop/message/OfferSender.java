package wsd.groupl.shop.message;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

public class OfferSender {

    public OfferSender() {}

    public ACLMessage getMessage(Double price, String receiver) {
        ACLMessage propose = new ACLMessage(ACLMessage.PROPOSE);
        AID receiverId = new AID(receiver, true);
        propose.addReceiver(receiverId);
        propose.setContent(price.toString());
        return propose;
    }

}
