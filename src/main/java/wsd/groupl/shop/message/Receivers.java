package wsd.groupl.shop.message;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class Receivers {
	public Receivers() {}
	public MessageTemplate offerRecieveTemplate(String agentName) {
		return MessageTemplate.and(MessageTemplate.MatchPerformative( ACLMessage.REQUEST ), MessageTemplate.MatchSender( new AID( agentName, AID.ISLOCALNAME)));
	}
}
