package wsd.groupl.shop;

import wsd.groupl.shop.behaviour.OfferBehaviour;
import jade.core.Agent;

public class ShopAgent extends Agent {

    @Override
    protected void setup() {
        System.out.println("Agent " + getAID().getName() + " is ready.");
        addBehaviour(new OfferBehaviour());
    }

    @Override
    protected void takeDown() {
        System.out.println("Agent " + getAID().getName() + " is dead");
    }
}
