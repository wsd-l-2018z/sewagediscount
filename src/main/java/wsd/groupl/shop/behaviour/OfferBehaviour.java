package wsd.groupl.shop.behaviour;

import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import wsd.groupl.shared.Logger;
import wsd.groupl.shop.message.OfferSender;
import wsd.groupl.shop.message.Receivers;
import wsd.groupl.shop.store.ProductStore;

public class OfferBehaviour extends CyclicBehaviour {
    private ProductStore productStore;
    private OfferSender offerSender;
    private Receivers recievers;
    public OfferBehaviour() {
        productStore = new ProductStore();
        offerSender = new OfferSender();
        recievers = new Receivers();
    }

    @Override
    public void action() {
    	String myName = myAgent.getLocalName().split("-")[0];
    	//name of neighbourhood agent is either under neighbourhood property or is first part of shop agent name
    	String neighbourhoodAgent = myAgent.getProperty("neighbourhood", myName + "-neighborhood-agent");
        ACLMessage msg = myAgent.receive(recievers.offerRecieveTemplate(neighbourhoodAgent));
        if (msg != null) {
        	productStore.initProducts();
            String product = msg.getContent();
            Double offer = productStore.getProductOffer(product);
            Logger.log("Sending offer for: " + product + " for: " + offer + " to: " + neighbourhoodAgent);
            ACLMessage message = offerSender.getMessage(offer, msg.getSender().getName());
            myAgent.send(message);
        } else {
            block();
        }
    }
}
