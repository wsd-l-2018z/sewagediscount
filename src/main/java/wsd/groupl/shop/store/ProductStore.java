package wsd.groupl.shop.store;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ProductStore {

    private Map<String, Double> products;

    public ProductStore() {}

    public void initProducts() {
        products = new HashMap<String, Double>();
        products.put("TOMATO", getPrice(5, 10));
        products.put("APPLE", getPrice(2, 4));
        products.put("HAM", getPrice(10, 15));
        products.put("BANANA", getPrice(5, 10));
        products.put("MINERAL_WATER", getPrice(5, 10));
        products.put("BROCCOLI", getPrice(5, 10));
    }

    private Double getPrice(double min, double max) {
        return new Random().doubles(min, (max + 1)).limit(1).findFirst().getAsDouble();
    }

    public Double getProductOffer(String key) {
        return products.get(key);
    }
}
