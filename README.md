# SewageDiscount

## Prerequisites

- Java 8

- Maven

## Running

### MainContainer

```
mvn -P main exec:java
```

### Run agent in container by providing its classpath

```
mvn -P container exec:java example-agent:wsd.groupl.shop.ShopAgent()  # example agent
```

### ProbeStationAgent

```
mvn -P probe-station exec:java
```

### ShopAgent

```
mvn -P shop exec:java
```

### NeighborhoodAgent

```
mvn -P neighborhood exec:java
```

### AdvertisementSpaceAgent

```
mvn -P ad exec:java
```
